const url = "https://lobinhos.herokuapp.com/wolves/";

/***
 *    ------------    ------    -----------  ----------   ------------ 
 *    ************   ********   ***********  ************ ************ 
 *    ---           ----------  ----    ---  --        -- ----         
 *    ***          ****    **** *********    **        ** ************ 
 *    ---          ------------ ---------    --        -- ------------ 
 *    ***          ************ ****  ****   **        **        ***** 
 *    ------------ ----    ---- ----   ----  ------------ ------------ 
 *    ************ ****    **** ****    **** **********   ************ 
 *                                                                     
 */

/*Cards*/

let sectionGeral = document.querySelector(".sectionGeral")

function createCards(element, side) {
    let sectionEscopo = document.createElement("section")
    sectionEscopo.classList.add("section-escopo")

    let sectionImg = document.createElement("section")
    let div = document.createElement("div")
    let img = document.createElement("img")
    sectionImg.classList.add("section-wolfs-img")
    img.src = `${element.link_image}`
    img.alt = "Imagem do lobo ao qual o texto se refere"
    sectionImg.append(div)
    sectionImg.append(img)

    let sectionTxt = document.createElement("section")
    let h2Name = document.createElement("h2")
    let h3Age = document.createElement("h3")
    let pDescription = document.createElement("p")
    let buttonAdotar = document.createElement("button")

    h2Name.innerText = `${element.name}`
    h3Age.innerText = `Idade: ${element.age} anos`
    pDescription.innerText = `${element.description}`
    buttonAdotar.innerText = "Adotar"
    sectionTxt.append(h2Name)
    sectionTxt.append(buttonAdotar)
    sectionTxt.append(h3Age)
    sectionTxt.append(pDescription)
    buttonAdotar.addEventListener("click", e => {
        e.preventDefault()
        window.location.href = `showLobinho.html?${element.id}`
    })


    if (side == 0) {
        sectionEscopo.append(sectionImg)
        sectionEscopo.append(sectionTxt)
        sectionTxt.classList.add("section-wolfs-txt")
    } else {
        sectionEscopo.append(sectionTxt)
        sectionEscopo.append(sectionImg)
        sectionTxt.classList.add("section-wolfs-txtRight")
        div.classList.add("divRight")
    }

    sectionGeral.append(sectionEscopo)

}

/***
 *    -----------     ------    ------------ --------  ----    ----    ------    ------------    ------      --------   
 *    ************   ********   ************ ********  *****   ****   ********   ************   ********    **********  
 *    ---      ---  ----------  ----           ----    ------  ----  ----------  ---           ----------  ----    ---- 
 *    ************ ****    **** ****  ******   ****    ************ ****    **** ***          ****    **** ***      *** 
 *    -----------  ------------ ----  ------   ----    ------------ ------------ ---          ------------ ---      --- 
 *    ****         ************ ****    ****   ****    ****  ****** ************ ***          ************ ****    **** 
 *    ----         ----    ---- ------------ --------  ----   ----- ----    ---- ------------ ----    ----  ----------  
 *    ****         ****    **** ************ ********  ****    **** ****    **** ************ ****    ****   ********   
 *                                                                                                                      
 */

/*Paginação*/

function pagItens(resp, pageActual) {
    let resul = []
    let totalPage = Math.ceil(resp.length / 4)
    let count = (pageActual * 4) - 4
    let delimiter = count + 4;

    if (pageActual <= totalPage) {
        for (let i = count; i < delimiter; i++) {
            resul.push(resp[i])
            count++
        }
    }

    let side = 0
    resul.forEach(element => {
        if (side == 0) {
            createCards(element, side)
            side = 1
        } else {
            createCards(element, side)
            side = 0
        }
    })

    return;
}

let pageActual = window.location.href.split("?")[1];

fetch(url)
    .then(resp => resp.json()
        .then(resp => {
            if (pageActual >= 1) {
                pagItens(resp, pageActual)
            } else {
                window.location.href = "listaDeLobinhos.html?1"
            }

        })
    )
    .catch(err => console.log(err))

/***
 *    -----------  ------------ ------------   --------   ----    ---- --------  ------------    ------    
 *    ************ ************ ************  **********  ****    **** ********  ************   ********   
 *    ---      --- ----         ----         ----    ---- ----    ----   ----            ----  ----------  
 *    ************ ************ ************ ***      *** ****    ****   ****    ************ ****    **** 
 *    -----------  ------------ ------------ ---   --  -- ----    ----   ----    ------------ ------------ 
 *    ****         ****                ***** ****   ** ** ************   ****    ****         ************ 
 *    ----         ------------ ------------  ------ -- - ------------ --------  ------------ ----    ---- 
 *    ****         ************ ************   ******* ** ************ ********  ************ ****    **** 
 *                                                                                                         
 */

/*Pesquisa*/

function search(text) {
    fetch(url)
        .then(resp => resp.json()
            .then(resp => {
                let cont = 0
                sectionGeral.innerHTML = "";
                resp.forEach(element => {
                    if (element.name.includes(text)) {
                        if (cont == 0) {
                            createCards(element, cont)
                            cont = 1
                        } else {
                            createCards(element, cont)
                            cont = 0
                        }
                    }
                })
            })
        )
        .catch(err => console.log(err))
}

/***
 *    -----------    --------   ------------   --------   ------------ ------------ 
 *    ***********   **********  ************  **********  ************ ************ 
 *    ----       - ----    ---- ------------ ----    ---- ----         ----         
 *    ***********  ***      ***     ****     ***      *** ************ ************ 
 *    -----------  ---      ---     ----     ---      --- ------------ ------------ 
 *    ****       * ****    ****     ****     ****    **** ****                ***** 
 *    -----------   ----------      ----      ----------  ------------ ------------ 
 *    ***********    ********       ****       ********   ************ ************ 
 *                                                                                  
 */

/*Event Listener dos botões*/

let buttonAdd = document.querySelector(".buttonAdd")
buttonAdd.addEventListener("click", (event) => {
    event.preventDefault();
    window.location.href = `addLobinho.html?`
})

let searchBar = document.querySelector("form")
searchBar.addEventListener("submit", (event) => {
    event.preventDefault();
    let input = document.querySelector(".input-text")
    let text = input.value
    search(text)
})

let buttonAnt = document.querySelector(".button-ant")
buttonAnt.addEventListener("click", () => {
    window.location.href = `listaDeLobinhos.html?${parseInt(pageActual)- 1}`

})

let buttonSeg = document.querySelector(".button-seg")
buttonSeg.addEventListener("click", () => {
    window.location.href = `listaDeLobinhos.html?${parseInt(pageActual)+ 1}`

})