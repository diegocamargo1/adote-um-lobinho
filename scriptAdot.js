const url = "https://lobinhos.herokuapp.com/wolves/";
let sectionGeral = document.querySelector(".sectionGeral")

function adotar() {
    let name = document.querySelector(".inputName").value
    let age = document.querySelector(".inputIdade").value
    let email = document.querySelector(".inputEmail").value

    let fetchBody = {
        "adoption": {
            "name": name,
            "email": email,
            "age": age,
            "wolf_id": parametroUrl,
        }
    }

    let fetchConfig = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(fetchBody)
    }

    fetch(url + "adoption", fetchConfig)
        .then(resp => resp.json()
            .then(resp => {
                document.querySelector("div").style.display = "block"
            })
        )
}


function createCards(element) {
    let sectionEscopo = document.createElement("section")
    sectionEscopo.classList.add("section-escopo")

    let sectionData = document.createElement("section")
    let sectionForm = document.createElement("section")

    let sectionImg = document.createElement("section")
    let img = document.createElement("img")
    let sectionTxt = document.createElement("section")
    let h1Name = document.createElement("h1")
    let pId = document.createElement("p")

    let sectionNameIdade = document.createElement("section")
    let labelName = document.createElement("label")
    let inputName = document.createElement("input")
    let labelIdade = document.createElement("label")
    let inputIdade = document.createElement("input")
    let labelEmail = document.createElement("label")
    let inputEmail = document.createElement("input")
    let buttonAdotar = document.createElement("button")


    img.src = `${element.link_image}`
    h1Name.innerText = `Adote o(a) ${element.name}`
    img.alt = "Imagem do lobo ao qual sera adotado"
    pId.innerText = `ID: ${element.id}`
    labelName.innerText = "Seu nome:"
    labelIdade.innerText = "Idade:"
    labelEmail.innerText = "E-mail:"
    buttonAdotar.innerText = "Adotar"



    sectionData.classList.add("sectionData")
    sectionForm.classList.add("sectionForm")
    sectionImg.classList.add("sectionImg")
    sectionTxt.classList.add("sectionTxt")
    sectionNameIdade.classList.add("sectionNameIdade")
    inputName.classList.add("inputName")
    inputIdade.classList.add("inputIdade")
    inputEmail.classList.add("inputEmail")

    buttonAdotar.addEventListener("click", e => {
        e.preventDefault()
        adotar()
    })

    sectionImg.append(img)
    sectionTxt.append(h1Name)
    sectionTxt.append(pId)
    sectionData.append(sectionImg)
    sectionData.append(sectionTxt)

    labelName.append(inputName)
    labelIdade.append(inputIdade)
    labelEmail.append(inputEmail)
    sectionNameIdade.append(labelName)
    sectionNameIdade.append(labelIdade)
    sectionForm.append(sectionNameIdade)
    sectionForm.append(labelEmail)
    sectionForm.append(buttonAdotar)

    sectionEscopo.append(sectionData)
    sectionEscopo.append(sectionForm)

    sectionGeral.append(sectionEscopo)

}

let parametroUrl = window.location.href.split("?")[1];

fetch(url + parametroUrl)
    .then(resp => resp.json()
        .then(resp => {
            createCards(resp)
        })
    )
    .catch(err => console.log(err))

let buttonContinuar = document.querySelector(".buttonContinuar")
buttonContinuar.addEventListener("click", () => {
    document.querySelector("div").style.display = "none"
    window.location.href = "listaDeLobinhos.html?1"
})