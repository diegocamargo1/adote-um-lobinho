const url = "https://lobinhos.herokuapp.com/wolves/";

let buttonSalvar = document.querySelector(".button-salvar")

buttonSalvar.addEventListener("click", e => {
    e.preventDefault()
    let name = document.querySelector(".input-name").value
    let age = document.querySelector(".input-age").value
    let img = document.querySelector(".input-img").value
    let desc = document.querySelector(".input-desc").value

    let fetchBody = {
        "wolf": {
            "name": name,
            "age": age,
            "link_image": img,
            "description": desc,
        }
    }
    console.log(fetchBody)

    let fetchConfig = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(fetchBody)
    }
    console.log(fetchConfig)

    fetch(url, fetchConfig)
        .then(resp => resp.json()
            .then(resp => {
                document.querySelector("div").style.display = "block"
            })
        )
})

let buttonContinuar = document.querySelector(".buttonContinuar")

buttonContinuar.addEventListener("click", e => { document.querySelector("div").style.display = "none" })